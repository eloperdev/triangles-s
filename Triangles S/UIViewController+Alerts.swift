//
//  UIViewController+Alerts.swift
//  Triangles S
//
//  Created by Luka on 08/12/2018.
//  Copyright © 2018 Luka Marčetić. All rights reserved.
//

import UIKit
extension UIViewController {
    func alert(_ message: String, callback:((UIAlertAction) -> Void)? = nil) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "OK", style: .default, handler: callback)
            alertController.addAction(alertAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
}
