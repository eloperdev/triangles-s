//
//  TriangleCell.swift
//  Triangles S
//
//  Created by Luka on 08/12/2018.
//  Copyright © 2018 Luka Marčetić. All rights reserved.
//

import UIKit
class TriangleCell : UITableViewCell{
    
    static private let dateFormatter = { () -> DateFormatter in
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.autoupdatingCurrent
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        return dateFormatter
    }()
    
    @IBOutlet private weak var parametersLabel: UILabel!
    @IBOutlet private weak var resultLabel: UILabel!
    @IBOutlet private weak var timeLabel: UILabel!
    
    func setModel(triangle: TriangleModel){
        let f = "%.4g"
        parametersLabel.text = String(format:f, triangle.a) + ", " + String(format:f, triangle.b) + ", " + String(format:f, triangle.c)
        resultLabel.text = String(format:f, triangle.area)
        timeLabel.text = type(of:self).dateFormatter.string(from: triangle.date)
    }
    
}
