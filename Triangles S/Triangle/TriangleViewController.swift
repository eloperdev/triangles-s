//
//  ViewController.swift
//  Triangles S
//
//  Created by Luka on 08/12/2018.
//  Copyright © 2018 Luka Marčetić. All rights reserved.
//

import UIKit

class TriangleViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var stackViewXLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var stackViewHeightLayoutConstraint: NSLayoutConstraint!
    //First page
    @IBOutlet weak var textFieldA: UITextField!
    @IBOutlet weak var textFieldB: UITextField!
    @IBOutlet weak var textFieldC: UITextField!
    //Second page
    @IBOutlet weak var tableView: UITableView!
    
    var selectedPage : Int = 0 {
        didSet{
            DispatchQueue.main.async {
                self.updateStackViewConstraints()
            }
        }
    }
    var triangles = SortedCollection<TriangleModel>(){$0.area >= $1.area}
    let trianglesJsonFileUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("triangles.json")
    let cellReuseIdentifier = "triangleCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let trianglesJsonFilePath = trianglesJsonFileUrl?.path, let data = FileManager.default.contents(atPath: trianglesJsonFilePath){
            if let trianglesElements = try? JSONDecoder().decode([TriangleModel].self, from: data){
                self.triangles = SortedCollection(orderedArray: trianglesElements){ $0.area >= $1.area }
            }
        }
        NotificationCenter.default.addObserver(forName: UIWindow.keyboardWillChangeFrameNotification, object: nil, queue: nil) {[weak self] (notification) in self?.keyboardWillChangeFrame(notification: notification) }
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        NotificationCenter.default.addObserver(forName: appDelegate?.applicationDidEnterBackground, object: nil, queue: nil) { (notification) in self.applicationDidEnterBackground(application:notification.userInfo!["application"] as! UIApplication) }
    }
    
    func keyboardWillChangeFrame(notification:Notification) {
        if let keyboardFrame = notification.userInfo?[UIWindow.keyboardFrameEndUserInfoKey] as? CGRect, let keyboardAnimationCurveInt = notification.userInfo?[UIWindow.keyboardAnimationCurveUserInfoKey] as? Int, let keyboardAnimationDuration = notification.userInfo?[UIWindow.keyboardAnimationDurationUserInfoKey] as? Double {
            let keyboardAnimationCurve = UIView.AnimationCurve(rawValue:keyboardAnimationCurveInt) ?? .easeInOut
            UIViewPropertyAnimator(duration: keyboardAnimationDuration, curve:keyboardAnimationCurve) {[weak self] in
                self?.stackViewHeightLayoutConstraint.constant = -keyboardFrame.size.height
                self?.view.layoutIfNeeded()
            }.startAnimation()
        }
    }
    
    func applicationDidEnterBackground(application:UIApplication){
        if let trianglesJsonFilePath = trianglesJsonFileUrl?.path {
            let trianglesData = try? JSONEncoder().encode(triangles.elements)
            FileManager.default.createFile(atPath: trianglesJsonFilePath, contents: trianglesData)
        }
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        updateStackViewConstraints()
    }
    
    func updateStackViewConstraints(){
        let page = stackView.subviews[selectedPage]
        stackViewXLayoutConstraint.constant = page.frame.origin.x
    }
    
    //MARK: Actions
    @IBAction func equalsButtonTouched(_ sender: Any) {
        guard let a = Double(textFieldA.text ?? ""), let b = Double(textFieldB.text ?? ""), let c = Double(textFieldC.text ?? "") else {
            alert("All fields must be filled with valid numbers")
            return
        }
        guard a > 0 && b > 0 && c > 0 else{
            alert("All numbers must be greater than 0")
            return
        }
        guard a + b > c  &&  a + c > b  &&  b + c > a else {
            alert("Lengths don't form a triangle: The sum of lengths of any two sides must be greater than the length of the remaining side.")
            return
        }
        let triangle = TriangleModel(a:a, b:b, c:c)
        let i = triangles.add(triangle)
        let indexPath = IndexPath(row: i, section: 0)
        tableView.insertRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: UITableView.ScrollPosition.middle)
        selectedPage += 1
    }
    
    @IBAction func backBarButtonItemTouched(_ sender: Any) {
        selectedPage -= 1
    }
    
    //MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return triangles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath)
        if let triangleCell = cell as? TriangleCell {
            triangleCell.setModel(triangle: triangles[indexPath.row])
        }
        return cell
    }
    
    //MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier)
        return cell
    }
}

