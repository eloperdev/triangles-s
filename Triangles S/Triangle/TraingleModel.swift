//
//  TraingleModel.swift
//  Triangles S
//
//  Created by Luka on 08/12/2018.
//  Copyright © 2018 Luka Marčetić. All rights reserved.
//

import Foundation
struct TriangleModel : Codable{
    
    public private(set) var a : Double
    public private(set) var b : Double
    public private(set) var c : Double
    public private(set) var area : Double
    public private(set) var date : Date
    
    init(a:Double, b:Double, c:Double) {
        (self.a, self.b, self.c) = (a, b, c)
        //Heron's formula:
        let s = a/2 + b/2 + c/2 //semiparameter: (a+b+c)/2
        area = (s * (s-a) * (s-b) * (s-c)).squareRoot()
        date = Date()
    }
}
