//
//  SortedCollection.swift
//  Triangles S
//
//  Created by Luka on 08/12/2018.
//  Copyright © 2018 Luka Marčetić. All rights reserved.
//

import Foundation
class SortedCollection<T> : Collection{
    typealias Element = T
    typealias Index = Array<Element>.Index
    
    public private(set) var elements : [Element]
    let comparator : (Element, Element) -> Bool
    
    init(orderedArray:[Element] = [], comparator: @escaping (Element, Element) -> Bool){
        elements = orderedArray
        self.comparator = comparator
    }
    
    @discardableResult func add(_ element:Element) -> Index{
        let i = binarySearch(forAPlaceFor: element)
        elements.insert(element, at: i)
        return i
    }
    
    func binarySearch(forAPlaceFor element:Element) -> Index{
        guard elements.count > 0 else {
            return 0
        }
        var low = 0
        var high = elements.count - 1
        var i = (low + high) / 2
        while low <= high {
            i = (low + high) / 2
            let currentElement = elements[i]
            if comparator(currentElement, element){
                low = i + 1
            } else if comparator(element, currentElement) {
                high = i - 1
            } else {
                return i
            }
        }
        return low
    }

    //MARK: Collection
    var startIndex: Index { return elements.startIndex }
    var endIndex: Index { return elements.endIndex }
    func index(after i: Index) -> Index {
        return elements.index(after: i)
    }
    subscript(position: Index) -> Element {
        get {
            return elements[position]
        }
    }
}
